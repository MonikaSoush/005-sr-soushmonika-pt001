import { Container } from "react-bootstrap";

export default function (props) {
  return (
    <div {...props} className="header">
      <Container className="d-flex p-3">
        <h5 className="m-0 ml-3">Category</h5>
      </Container>
    </div>
  );
}
