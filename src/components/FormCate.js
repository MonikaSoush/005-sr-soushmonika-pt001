import { useFormik } from "formik";
import { useEffect } from "react";
import { Col, Form, Row, Button } from "react-bootstrap";
import * as Yup from "yup";
export default function (props) {
  const { category, onSubmit, onCancel, isEdit } = props;
  const validationSchema = Yup.object().shape({
    name: Yup.string()
      .matches("^[a-zA-Z\\s]+$", "Name allow character only")
      .required("Name is required"),
  
  });
  const {
    values,
    handleChange,
    handleSubmit,
    errors,
    setFieldValue,
    setValues,
  } = useFormik({
    validationSchema,
    onSubmit,
    initialValues: isEdit
      ? category
      : {
          name: "",

        },
    validateOnChange: false,
  });
  useEffect(() => {
    setValues(
      isEdit
        ? category
        : {
            name: "",
          }
    );
  }, [isEdit, category]);
  return (
    <div {...props}>
      <h1 className="font-weight-normal my-3">Category</h1>
      <Form onSubmit={handleSubmit}>
        <Row>
          <Col>
            <Form.Group>
              <Form.Label>Category Name</Form.Label>
              <Form.Control
                type="text"
                onChange={handleChange}
                value={values.name}
                placeholder="Enter name"
                name="name"
                isInvalid={!!errors.name}
              />
              <Form.Control.Feedback type="invalid">
                {errors.name}
              </Form.Control.Feedback>
            </Form.Group>
      
            <div className="d-flex justify-content-end">
              <Button variant="primary" type="submit">
                {isEdit ? "Save" : "Add"}
              </Button>
              {isEdit && (
                <Button className="ml-3" variant="secondary" onClick={onCancel}>
                  Cancel
                </Button>
              )}
            </div>
          </Col>
        </Row>
      </Form>
    </div>
  );
}
