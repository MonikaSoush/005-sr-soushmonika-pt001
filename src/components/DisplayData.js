import { useEffect, useState } from "react";
import {
  ButtonGroup,
  Table,
  Card,
  Row,
  Col,
  Dropdown,
  ToggleButton,
  Button,
} from "react-bootstrap";
import moment from "moment";
import { MdVisibility, MdEdit, MdDelete } from "react-icons/md";

export default function (props) {
  const perPage = 3;
  const [listStyle, setListStyle] = useState("table");
  const [list, setList] = useState();
  const { category = [], isEdit, onAction } = props;
  const [currentPage, setCurrentPage] = useState(1);
  const [categoryList, setPersonList] = useState([]);

  useEffect(() => {
    renderLayout();
  }, [listStyle, isEdit, categoryList]);

  useEffect(() => {
    setPersonList(getCategoryByPage(currentPage));
  }, [category, currentPage]);

  function getTotalPage() {
    return Math.ceil(category.length / perPage);
  }
  function onPageChange(page) {
    setCurrentPage(page);
    setPersonList(getCategoryByPage(page));
  }

  function getCategoryByPage(page) {
    const indexOfLast = page * perPage;
    const indexOfFirst = indexOfLast - perPage;
    return category.slice(indexOfFirst, indexOfLast);
  }
  function onListChange(e) {
    const type = e.currentTarget.value;
    setListStyle(type);
  }
  function renderLayout() {
    switch (listStyle) {
      case "table":
        setList(tableStyle());
        break;
      case "card":
        setList(cardStyle());
        break;
    }
  }
  function cardStyle() {
    return <Row className="flex-wrap">{categoryList.map(renderCard)}</Row>;
  }
  function renderCard(item, index) {
    return (
      <Col className="col-3 mb-4" key={index}>
        <Card>
          <Card.Header className="d-flex justify-content-center">
            <Dropdown onSelect={(e) => onAction(e, item)}>
              <Dropdown.Toggle size="sm" variant="success">
                Action
              </Dropdown.Toggle>
              <Dropdown.Menu>
                
                <Dropdown.Item eventKey="edit">Edit</Dropdown.Item>
                <Dropdown.Item disabled={isEdit} eventKey="delete">
                  Delete
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </Card.Header>
          <Card.Body>
            <h5>{item.name}</h5>
          </Card.Body>
          <Card.Footer className="d-flex justify-content-center">
          </Card.Footer>
        </Card>
      </Col>
    );
  }
  function renderRow(item, index) {
    return (
      <tr key={index}>
        <td>{item.id}</td>
        <td>{item.name}</td>
          <div className="d-flex">
    
            <Button
              className="d-flex align-items-center mr-2"
              size="sm"
              variant="secondary"
              onClick={() => onAction("edit", item)}
            >
              <MdEdit className="mr-2" />
              Edit
            </Button>
            <Button
              disabled={isEdit}
              className="d-flex align-items-center"
              size="sm"
              variant="danger"
              onClick={() => onAction("delete", item)}
            >
              <MdDelete className="mr-2" />
              Delete
            </Button>
          </div>
       
      </tr>
    );
  }
  function tableStyle() {
    return (
      <Table bordered>
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>{categoryList.map(renderRow)}</tbody>
      </Table>
    );
  }
  return (
    <div>
     
      {list}
      
    </div>
  );
}
