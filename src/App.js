import React from "react";
import Header from "./components/Header";
import { Container } from "react-bootstrap";
import FormCategory from "./components/FormCate";
import DisplayData from "./components/DisplayData";


import "./App.css";

class CategoryInformation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isEdit: false,

      category: undefined,
      categorys: [
        {
          id: 1,
          name: "Mocha",
        },
      
      ],
    };
  }
  onAction = (type, item) => {
    switch (type) {
      case "edit":
        this.onEdit(item);
        break;
      case "delete":
        this.onDelete(item);
        break;
    }
  };
  genId = () => {
    const { categorys } = this.state;
    if (categorys.length == 0) return 1;
    return categorys.map((item) => item.id).reduce((a, b) => Math.max(a, b)) + 1;
  };
  onEdit = (category) => {
    this.setState({ isEdit: true, category });
  };
  onCancelEdit = () => {
    this.setState({ isEdit: false });
  };
  onSubmit = (value) => {
    const { categorys, isEdit } = this.state;
    if (isEdit) {
      this.setState({
        isEdit: false,
        categorys: [
          ...categorys.map((item) =>
            item.id === value.id ? { ...value, updateAt: Date.now() } : item
          ),
        ],
      });
    } else {
      this.setState({
        isEdit: false,
        categorys: [...categorys, { ...value, id: this.genId() }],
      });
    }
  };
  onDelete = (category) => {
    const { categorys } = this.state;
    this.setState({
      categorys: [...categorys.filter((item) => item.id !== category.id)],
    });
  };

  render() {
    const { categorys, category, isEdit, currentPage } = this.state;
    return (
      <div>
        <Header />
        <Container>
          <FormCategory
            onSubmit={this.onSubmit}
            isEdit={isEdit}
            category={category}
            onCancel={this.onCancelEdit}
          />
          <DisplayData
            isEdit={isEdit}
            onAction={this.onAction}
            categorys={categorys}
          />
         
          )}
        </Container>
      </div>
    );
  }
}

export default CategoryInformation;
